﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harjoitus1
{
    public class Circle
    {
        private double radius;
        private String color;

        // The default constructor with no argument.
        // It sets the radius and color to their default value.
        public Circle()
        {
            radius = 1.0;
            color = "red";
        }

        // 2nd constructor with given radius, but color default
        public Circle(double radius)
        {
            this.radius = radius;
            color = "red";
        }

        public Circle(double radius, String color)
        {
            this.radius = radius;
            this.color = color;
        }

        // A public method for retrieving the radius
        public double getRadius()
        {
            return radius;
        }

        public String getColor()
        {
            return color;
        }

        // A public method for computing the area of circle
        public double getArea()
        {
            return radius * radius * Math.PI;
        }

        public override string ToString()
        {
            return radius + " " + color;
        }

        public void setColor(String color)
        {
            this.color = color;
        }

        public void setRadius(double radius)
        {
            this.radius = radius;
        }
    }
}
